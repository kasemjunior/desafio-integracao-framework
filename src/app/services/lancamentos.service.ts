import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Consolidado } from '../models/consolidado';
import { Lancamento } from '../models/lancamento';

@Injectable({
  providedIn: 'root'
})
export class LancamentosService {

  constructor(private http: HttpClient) { }

  /**
   * Carrega os lançamentos realizados do cartão
   */
  carregarLancamentos(): Observable<Lancamento[]> {

    const lancamentosEndpoint = `${environment.api_endpoint}/lancamentos`;

    return this
      .http
      .get(lancamentosEndpoint)
      .pipe(
        map<any, Lancamento[]>(lancamentos =>
          lancamentos.map(json => this.fromJson(json)))
      );

  }

  /**
   * Consolida os lançamentos por mês e categoria
   *
   * @param lancamentos Lançamentos a ser consolidados
   */
  consolidarLancamentos(lancamentos: Lancamento[]): Observable<Consolidado[]> {

    const consolidadoMesCategoria: { [key: string]: Consolidado } = lancamentos
      .reduce((consolidado: { [key: string]: Consolidado }, lancamento: Lancamento) => {

        const mesCategoria = `${lancamento.mes}|${lancamento.categoria}`;

        consolidado[mesCategoria] = consolidado[mesCategoria] ?? <Consolidado>{
          mes: lancamento.mes,
          categoria: lancamento.categoria,
          total: 0
        };

        consolidado[mesCategoria].total += lancamento.valor;
        consolidado[mesCategoria].total = this.corrigirFlutuante(consolidado[mesCategoria].total);

        return consolidado;

      }, {});

    let consolidado: Consolidado[] = [];

    Object
      .keys(consolidadoMesCategoria)
      .map(key => consolidado.push(consolidadoMesCategoria[key]));

    consolidado = consolidado.sort((a, b) => this.ordernarMes(a, b));

    return of(consolidado);

  }

  /**
   * Corrige o ponto flutuante
   *
   * @param valor
   */
  private corrigirFlutuante(valor: number): number {

    return Math.round(valor * 100) / 100;

  }

  /**
   * Ordena os consolidados pelo mês
   * @param a
   * @param b
   */
  private ordernarMes(a: Consolidado, b: Consolidado): number {
    if (a.mes > b.mes) {
      return 1;
    }

    if (a.mes < b.mes) {
      return -1;
    }

    return 0;
  }

  /**
   * @param json Json contendo os dados do lançamento
   *
   * @returns O lançamento com os dados preenchidos
   */
  private fromJson(json: any): Lancamento {

    const lancamento: Lancamento = new Lancamento();

    lancamento.id = json.id;
    lancamento.valor = json.valor;
    lancamento.origem = json.origem;
    lancamento.categoria = json.categoria;
    lancamento.mes = json.mes_lancamento;

    return lancamento;

  }

}
