import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Categoria } from '../models/categoria';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  constructor(private http: HttpClient) { }

  /**
   * Carrega as categorias de lançamentos
   */
  carregarCategorias(): Observable<Categoria[]> {

    const categoriasEndpoint = `${environment.api_endpoint}/categorias`;

    return this
      .http
      .get(categoriasEndpoint)
      .pipe(
        map<any, Categoria[]>(categorias =>
          categorias.map(json => this.fromJson(json)))
      );

  }

  /**
   * @param json Json contendo os dados da categoria
   *
   * @returns A categoria com os dados preenchidos
   */
  private fromJson(json: any): Categoria {

    const categoria: Categoria = new Categoria();

    categoria.id = json.id;
    categoria.nome = json.nome;

    return categoria;

  }

}
