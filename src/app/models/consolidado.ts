/**
 * Classe que representa os dados consolidados
 */
export class Consolidado {
  mes: number;
  categoria: number;
  total: number;
}
