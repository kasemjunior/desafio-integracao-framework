import { NomeMesPipe } from './nome-mes.pipe';

describe('NomeMesPipe', () => {

  it('deve criar uma instancia', () => {
    const pipe = new NomeMesPipe('pt');
    expect(pipe).toBeTruthy();
  });

  it('deve obter o nome do mês por extenso', () => {
    const pipe = new NomeMesPipe('pt');
    const nomeMes = pipe.transform(1);
    expect(nomeMes).toEqual('Janeiro');
  });

});
