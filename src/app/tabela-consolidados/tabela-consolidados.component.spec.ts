import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NomeMesPipe } from '../pipes/nome-mes.pipe';
import { TabelaConsolidadosComponent } from './tabela-consolidados.component';
import { Consolidado } from '../models/consolidado';
import { Categoria } from '../models/categoria';
import { By } from '@angular/platform-browser';

registerLocaleData(localePt, 'pt');

describe('TabelaConsolidadosComponent', () => {
  let component: TabelaConsolidadosComponent;
  let fixture: ComponentFixture<TabelaConsolidadosComponent>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [
          TabelaConsolidadosComponent,
          NomeMesPipe
        ],
        providers: [
          { provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL' },
          { provide: LOCALE_ID, useValue: 'pt' }
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaConsolidadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  const gerarConsolidado = (
    mes: number,
    categoria: number,
    total: number) => {

    const consolidado = new Consolidado();
    consolidado.mes = mes;
    consolidado.categoria = categoria;
    consolidado.total = total;

    return consolidado;
  };

  const gerarCategoria = (id: number, nome: string): Categoria => {
    const categoria = new Categoria();
    categoria.id = id;
    categoria.nome = nome;

    return categoria;
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deve exibir o total do mês/categoria formatado', () => {

    const lancamentoStub: Consolidado = gerarConsolidado(1, 1, 1234.56);

    component.categorias = new Map<number, Categoria>();
    component.consolidados = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.consolidado-total'))
      .nativeElement
      .innerHTML
      .replace('&nbsp;', ' ')
      .trim();

    expect(textoCelula).toEqual('R$ 1.234,56');

  });

  it('deve exibir o nome do mês', () => {

    const categoriaStub: Categoria = gerarCategoria(1, 'Transporte');
    const lancamentoStub: Consolidado = gerarConsolidado(1, 1, 1234.56);

    component.categorias = new Map<number, Categoria>();
    component.consolidados = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.consolidado-mes'))
      .nativeElement
      .textContent
      .trim();

    expect(textoCelula).toEqual('Janeiro');

  });

  it('deve exibir o nome da categoria', () => {

    const categoriaStub: Categoria = gerarCategoria(1, 'Transporte');
    const categoriasMaps: Map<number, Categoria> = new Map<number, Categoria>()
    categoriasMaps.set(categoriaStub.id, categoriaStub);

    const lancamentoStub: Consolidado = gerarConsolidado(1, 1, 1234.56);

    component.categorias = categoriasMaps;
    component.consolidados = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.consolidado-categoria'))
      .nativeElement
      .textContent
      .trim();

    expect(textoCelula).toEqual(categoriaStub.nome);

  });

  it('deve exibir "Não categorizado" quando a categoria não for encontrada', () => {

    const lancamentoStub: Consolidado = gerarConsolidado(1, 1, 1234.5600);

    component.categorias = new Map<number, Categoria>();
    component.consolidados = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.consolidado-categoria'))
      .nativeElement
      .textContent
      .trim();

    expect(textoCelula).toEqual('Não categorizado');

  });

});
