import { Component, Input } from '@angular/core';
import { Categoria } from '../models/categoria';
import { Consolidado } from '../models/consolidado';

@Component({
  selector: 'app-tabela-consolidados',
  templateUrl: './tabela-consolidados.component.html'
})
export class TabelaConsolidadosComponent {

  @Input() categorias: ReadonlyMap<number, Categoria>;
  @Input() consolidados: Consolidado[];

}
