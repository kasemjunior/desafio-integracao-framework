import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { flatMap, share, map } from 'rxjs/operators';
import { Categoria } from './models/categoria';
import { Consolidado } from './models/consolidado';
import { Lancamento } from './models/lancamento';
import { CategoriasService } from './services/categorias.service';
import { LancamentosService } from './services/lancamentos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  categorias$: Observable<ReadonlyMap<number, Categoria>> = of();
  lancamentos$: Observable<Lancamento[]> = of();
  consolidados$: Observable<Consolidado[]> = of();

  constructor(
    private lancamentosService: LancamentosService,
    private categoriasService: CategoriasService) { }

  ngOnInit(): void {

    this.categorias$ = this.categoriasService.carregarCategorias()
      .pipe(
        map(categorias => this.converterCategoriasParaMap(categorias))
      );

    this.lancamentos$ = this.lancamentosService.carregarLancamentos()
      .pipe(share());

    this.consolidados$ = this
      .lancamentos$
      .pipe(
        flatMap<Lancamento[], Observable<Consolidado[]>>(lancamentos =>
          this.lancamentosService.consolidarLancamentos(lancamentos))
      );

  }

  private converterCategoriasParaMap(categorias: Categoria[]): ReadonlyMap<number, Categoria> {

    return categorias
      .reduce<Map<number, Categoria>>((map, categoria) => {
        map.set(categoria.id, categoria);
        return map;
      }, new Map<number, Categoria>());;

  }

}
